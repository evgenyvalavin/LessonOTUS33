﻿using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LessonOTUS33
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "buffalo.rmq.cloudamqp.com",
                VirtualHost = "vcxknihg",
                UserName = "vcxknihg",
                Password = "p7TsUYAC9fm8RbaMAFlQ2bGORgMk3F3M"
            };
            using (var conn = factory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                channel.QueueDeclare(
                    queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
                do
                {
                    var user = new User
                    {
                        Name = "Kelly",
                        Age = DateTime.Now.Second,
                        Email = $"kelly-{DateTime.Now.ToShortTimeString()}@outlook.com"
                    };
                    channel.BasicPublish(exchange: "",
                        routingKey: "homework",
                        basicProperties: null,
                        body: Encoding.UTF8.GetBytes(JsonSerializer.Serialize(user)));
                    Console.WriteLine(DateTime.Now.ToShortTimeString() + " Message sent");
                    await Task.Delay(1000);
                } while (true);
            }
        }
    }

    struct User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
    }
}
