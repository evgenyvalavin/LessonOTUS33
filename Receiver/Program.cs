﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Receiver
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "buffalo.rmq.cloudamqp.com",
                VirtualHost = "vcxknihg",
                UserName = "vcxknihg",
                Password = "p7TsUYAC9fm8RbaMAFlQ2bGORgMk3F3M"
            };
            using (var conn = factory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                channel.QueueDeclare(
                    queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
                var consumer = new EventingBasicConsumer(channel);
                User deserialized = default;
                consumer.Received += (model, cargs) =>
                {
                    var json = Encoding.UTF8.GetString(cargs.Body);
                    deserialized = JsonSerializer.Deserialize<User>(json);
                    Console.WriteLine(DateTime.Now.ToShortTimeString() + " Message received. " + json);
                    if (string.IsNullOrEmpty(deserialized.Email) is false)
                        channel.BasicAck(cargs.DeliveryTag, false);
                };
                channel.BasicConsume(
                queue: "homework",
                autoAck: false,
                consumer: consumer);
                Console.ReadKey();
            }
        }

        private static void Channel_BasicAcks(object sender, BasicAckEventArgs e)
        {
            throw new NotImplementedException();
        }
    }

    struct User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
    }
}
